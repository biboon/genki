package genki_test

import (
	"context"

	"gitlab.com/biboon/genki"
)

var _ genki.Pinger = genki.PingerFunc(nil)

func Pinger(b bool) genki.PingerFunc {
	return func(context.Context) bool { return b }
}
