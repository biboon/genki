package genki_test

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"gitlab.com/biboon/genki"
)

func TestReady(t *testing.T) {
	testCases := []struct {
		desc   string
		setup  func()
		ready  genki.Ready
		status int
	}{
		{
			desc:   "before default",
			setup:  func() { genki.DefaultReadyDate = time.Now().Add(time.Second) },
			ready:  genki.Ready{},
			status: http.StatusInternalServerError,
		},
		{
			desc:   "after default",
			setup:  func() { genki.DefaultReadyDate = time.Now() },
			ready:  genki.Ready{},
			status: http.StatusOK,
		},
		{
			desc:   "before custom",
			ready:  genki.Ready{time.Now().Add(time.Second)},
			status: http.StatusInternalServerError,
		},
		{
			desc:   "after custom",
			ready:  genki.Ready{time.Now()},
			status: http.StatusOK,
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			if tC.setup != nil {
				tC.setup()
			}

			w := httptest.NewRecorder()
			r := httptest.NewRequest("", "/", http.NoBody)

			tC.ready.ServeHTTP(w, r)

			assert.Equal(t, tC.status, w.Result().StatusCode)
		})
	}
}
