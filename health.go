package genki

import (
	"encoding/json"
	"net/http"
	"sync"
)

type Health map[string]Pinger

func (h Health) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	type P struct {
		string
		bool
	}

	wg, pings, index := new(sync.WaitGroup), make([]P, len(h)), 0
	for name, pinger := range h {
		wg.Add(1)
		go func(i int, n string, p Pinger) {
			defer wg.Done()
			pings[i] = P{n, p.Ping(r.Context())}
		}(index, name, pinger)

		index++
	}
	wg.Wait()

	checks, status, statusCode := make(map[string]bool, len(h)), true, http.StatusOK
	for _, ping := range pings {
		if name := ping.string; ping.bool {
			checks[name] = true
		} else {
			checks[name], status, statusCode = false, false, http.StatusInternalServerError
		}
	}

	w.Header().Set("content-type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(map[string]any{
		"status": status,
		"checks": checks,
	})
}
