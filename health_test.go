package genki_test

import (
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/biboon/genki"
)

var _ http.Handler = genki.Health{}

func TestHealth(t *testing.T) {
	testCases := []struct {
		desc   string
		health genki.Health
		code   int
		body   string
	}{
		{
			desc:   "empty",
			health: genki.Health{},
			code:   http.StatusOK,
			body: `{
				"status": true,
				"checks": {}
			}`,
		},
		{
			desc: "one true",
			health: genki.Health{
				"A": Pinger(true),
			},
			code: http.StatusOK,
			body: `{
				"status": true,
				"checks": {
					"A": true
				}
			}`,
		},
		{
			desc: "one false",
			health: genki.Health{
				"A": Pinger(false),
			},
			code: http.StatusInternalServerError,
			body: `{
				"status": false,
				"checks": {
					"A": false
				}
			}`,
		},
		{
			desc: "mixed",
			health: genki.Health{
				"A": Pinger(false),
				"B": Pinger(false),
				"C": Pinger(true),
			},
			code: http.StatusInternalServerError,
			body: `{
				"status": false,
				"checks": {
					"A": false,
					"B": false,
					"C": true
				}
			}`,
		},
	}

	for _, tC := range testCases {
		t.Run(tC.desc, func(t *testing.T) {
			w := httptest.NewRecorder()
			r := httptest.NewRequest("", "/", http.NoBody)

			tC.health.ServeHTTP(w, r)

			bytes, err := io.ReadAll(w.Result().Body)
			if err != nil {
				t.Fatal(err)
			}
			defer w.Result().Body.Close()

			assert.Contains(t, w.Result().Header.Values("content-type"), "application/json")
			assert.Equal(t, tC.code, w.Result().StatusCode)
			assert.JSONEq(t, tC.body, string(bytes))
		})
	}
}
