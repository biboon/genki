package genki

import (
	"net/http"
	"time"
)

type Ready struct{ Date time.Time }

var DefaultReadyDate = time.Now().Add(3 * time.Second)

func (h Ready) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var ready bool
	if h.Date.IsZero() {
		ready = time.Now().After(DefaultReadyDate)
	} else {
		ready = time.Now().After(h.Date)
	}

	statusCode := http.StatusOK
	if !ready {
		statusCode = http.StatusInternalServerError
	}

	w.WriteHeader(statusCode)
}
